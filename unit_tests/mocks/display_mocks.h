/*
 MIT License

 Copyright (c) 2018 Jacek Jankowski

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
#ifndef DISPLAY_MOCKS_H_
#define DISPLAY_MOCKS_H_

#include "fff.h"
DEFINE_FFF_GLOBALS
;

#include "icon_types.h"
#include "geometry_types.h"

static void Display_Init();
static void Display_Clear();
static void Display_DrawUnderBedLightIcon(Square_GeometryTypeDef Geometry);
static void Display_DrawChandelierIcon(Square_GeometryTypeDef Geometry);
static uint32_t Display_GetMaxX();
static uint32_t Display_GetMaxY();

static void Display_Init_reset();
static void Display_Clear_reset();
static void Display_DrawUnderBedLightIcon_reset();
static void Display_DrawChandelierIcon_reset();
static void Display_GetMaxX_reset();
static void Display_GetMaxY_reset();

FAKE_VOID_FUNC(Display_Init);
FAKE_VOID_FUNC(Display_Clear);
FAKE_VOID_FUNC(Display_DrawUnderBedLightIcon, Square_GeometryTypeDef);
FAKE_VOID_FUNC(Display_DrawChandelierIcon, Square_GeometryTypeDef);
FAKE_VALUE_FUNC(uint32_t, Display_GetMaxX);
FAKE_VALUE_FUNC(uint32_t, Display_GetMaxY);

static void display_mocks_reset() {
	Display_Init_reset();
	Display_Clear_reset();
	Display_DrawUnderBedLightIcon_reset();
	Display_DrawChandelierIcon_reset();
	Display_GetMaxX_reset();
	Display_GetMaxY_reset();
}

#endif /* DISPLAY_MOCKS_H_ */
