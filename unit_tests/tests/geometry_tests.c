/*
MIT License

Copyright (c) 2018 Jacek Jankowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

#define MUNIT_ENABLE_ASSERT_ALIASES
#include "munit.h"

#include "geometry.c"

static MunitResult test_inside_square(const MunitParameter params[], void* data) {
	Square_GeometryTypeDef Square;
	Square.Size = 3;
	Square.Position.X = 1;
	Square.Position.Y = 1;
	Point_GeometryTypeDef Point;

	Point.X = 2;
	Point.Y = 2;
	assert(Square_IsInside(Square,Point) == true);

	Point.X = 5;
	Point.Y = 5;
	assert(Square_IsInside(Square, Point) == false);

	Point.X = 4;
	Point.Y = 4;
	assert(Square_IsInside(Square, Point) == false);

	return MUNIT_OK;
}

static MunitTest geometry_tests[] = {
	{
		"/inside_square",
		test_inside_square,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

const MunitSuite geometry_suite = {
  (char*) "/geometry",
  geometry_tests,
  NULL,
  1,
  MUNIT_SUITE_OPTION_NONE
};
