/*
 MIT License

 Copyright (c) 2018 Jacek Jankowski

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#define MUNIT_ENABLE_ASSERT_ALIASES
#include "munit.h"

#include "geometry_types.h"
#include "fff.h"
DEFINE_FFF_GLOBALS;
static void Draw(Square_GeometryTypeDef);
FAKE_VOID_FUNC(Draw, Square_GeometryTypeDef);
static void React();
FAKE_VOID_FUNC(React);

#include "icon.c"

static MunitResult test_draw(const MunitParameter params[], void* data) {
	// given
	Icon_HandleTypeDef Icon;
	Square_GeometryTypeDef Geometry;
	Icon_Init(&Icon, Geometry, Draw, React);
	RESET_FAKE(Draw);
	// when
	Icon_Draw(&Icon);
	// then
	assert(Draw_fake.call_count == 1);
	return MUNIT_OK;
}

static MunitResult test_press(const MunitParameter params[], void* data) {
	Square_GeometryTypeDef Geometry;
	Geometry.Position.X = 1;
	Geometry.Position.Y = 1;
	Geometry.Size = 3;
	Icon_HandleTypeDef Icon;
	Icon_Init(&Icon, Geometry, Draw, React);
	Point_GeometryTypeDef Point;

	Point.X = 2;
	Point.Y = 2;
	assert(Icon_WasPressedDown(&Icon, Point) == true);

	Point.X = 4;
	Point.Y = 2;
	assert(Icon_WasPressedDown(&Icon, Point) == false);

	Point.X = 5;
	Point.Y = 2;
	assert(Icon_WasPressedDown(&Icon, Point) == false);

	return MUNIT_OK;
}

static MunitResult test_react_to_touch(const MunitParameter params[],
		void* data) {
	Square_GeometryTypeDef IconGeometry;
	IconGeometry.Position.X = 1;
	IconGeometry.Position.Y = 1;
	IconGeometry.Size = 3;
	Icon_HandleTypeDef Icon;
	Icon_Init(&Icon, IconGeometry, Draw, React);
	Point_GeometryTypeDef Point;

	assert(React_fake.call_count == 0);

	Point.X = IconGeometry.Position.X + IconGeometry.Size / 2;
	Point.Y = IconGeometry.Position.X + IconGeometry.Size / 2;
	Icon_ReactToTouch(&Icon, Point);
	assert(React_fake.call_count == 1);

	Point.X = IconGeometry.Position.X + IconGeometry.Size;
	Point.Y = IconGeometry.Position.X + IconGeometry.Size;
	assert(React_fake.call_count == 1);

	Point.X = IconGeometry.Position.X + IconGeometry.Size + 1;
	Point.Y = IconGeometry.Position.X + IconGeometry.Size + 1;
	assert(React_fake.call_count == 1);

	return MUNIT_OK;
}

static MunitTest icon_tests[] = {
	{
		"/draw",
		test_draw,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/press",
		test_press,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/react_to_touch",
		test_react_to_touch,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

const MunitSuite icon_suite = {
	(char*) "/icon",
	icon_tests,
	NULL,
	1,
	MUNIT_SUITE_OPTION_NONE
};
