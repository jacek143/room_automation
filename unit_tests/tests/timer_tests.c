/*
 MIT License

 Copyright (c) 2018 Jacek Jankowski

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#define MUNIT_ENABLE_ASSERT_ALIASES
#include "munit.h"

#include "timer.c"

static MunitResult test_restart(const MunitParameter params[], void* data) {
	Timer_HandleTypeDef Timer;
	const uint32_t Timeout = 10;

	Timer_Restart(&Timer, 0);
	assert(Timer_IsElapsed(&Timer));
	for (int i = 0; i < 3; i++) {
		Timer_Run_1ms(&Timer);
		assert(Timer_IsElapsed(&Timer));
	}

	Timer_Restart(&Timer, Timeout);
	for (int i = 0; i < Timeout; i++) {
		assert_false(Timer_IsElapsed(&Timer));
		Timer_Run_1ms(&Timer);
	}
	assert(Timer_IsElapsed(&Timer));

	return MUNIT_OK;
}

static MunitTest timer_tests[] = {
	{
		"/restart",
		test_restart,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

const MunitSuite timer_suite = {
  (char*) "/timer",
  timer_tests,
  NULL,
  1,
  MUNIT_SUITE_OPTION_NONE
};
