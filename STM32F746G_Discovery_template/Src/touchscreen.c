/*
 MIT License

 Copyright (c) 2018 Jacek Jankowski

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "touchscreen.h"
#include "main.h"
#include "stm32746g_discovery_ts.h"

static TS_StateTypeDef Touchscreen_State;

void Touchscreen_Init() {
	BSP_TS_Init(RK043FN48H_WIDTH, RK043FN48H_HEIGHT);
}

void Touchscreen_Run_1_ms() {
	BSP_TS_GetState(&Touchscreen_State);
}

bool Touchscreen_WasScreenPressedDown() {
	return Touchscreen_State.touchDetected
			&& Touchscreen_State.touchEventId[0] == TOUCH_EVENT_PRESS_DOWN;
}

Point_GeometryTypeDef Touchscreen_GetTouchCoordinates() {
	Point_GeometryTypeDef Point = { .X = Touchscreen_State.touchX[0], .Y =
			Touchscreen_State.touchY[0] };
	return Point;
}
