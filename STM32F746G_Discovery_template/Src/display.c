/*
 MIT License

 Copyright (c) 2018 Jacek Jankowski

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "display.h"
#include "stm32746g_discovery_lcd.h"
#include "icon.h"

static void Display_DrawIconsBackground(Square_GeometryTypeDef Geometry);
static void Display_DrawIconsFrame(Square_GeometryTypeDef Geometry);

void Display_Init() {
	BSP_LCD_Init();
	BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
	BSP_LCD_DisplayOn();
	BSP_LCD_SelectLayer(0);
}

void Display_Clear() {
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_SetTransparency(0, 0xff);
}

void Display_DrawUnderBedLightIcon(Square_GeometryTypeDef Geometry) {
	const Point_GeometryTypeDef Pos = Geometry.Position;
	const uint32_t Unit = Geometry.Size / 10;

	Display_DrawIconsBackground(Geometry);

	// snoring
	BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_SetFont(&Font12);
	BSP_LCD_DisplayStringAt(Pos.X + 3 * Unit, Pos.Y + 3 * Unit,
			(uint8_t*) "ZZZ", LEFT_MODE);

	// pillow
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_FillCircle(Pos.X + 2 * Unit, Pos.Y + 5 * Unit, Unit - 1);

	// quilt
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_FillRect(Pos.X + 4 * Unit, Pos.Y + 4 * Unit, 4 * Unit, Unit);
	BSP_LCD_FillCircle(Pos.X + 4 * Unit, Pos.Y + 5 * Unit, Unit - 1);
	BSP_LCD_FillCircle(Pos.X + 8 * Unit, Pos.Y + 5 * Unit, Unit - 1);

	// bed frame
	uint32_t CustomBrownColor = 0xFF8B4513;
	BSP_LCD_SetTextColor(CustomBrownColor);
	BSP_LCD_FillRect(Pos.X + 1 * Unit, Pos.Y + 3 * Unit, Unit, 4 * Unit);
	BSP_LCD_FillRect(Pos.X + 2 * Unit, Pos.Y + 5 * Unit, 6 * Unit, Unit);
	BSP_LCD_FillRect(Pos.X + 8 * Unit, Pos.Y + 5 * Unit, Unit, 2 * Unit);

	Display_DrawIconsFrame(Geometry);
}

void Display_DrawChandelierIcon(Square_GeometryTypeDef Geometry) {
	const uint32_t Unit = Geometry.Size / 6;
	const Point_GeometryTypeDef Position = Geometry.Position;

	Display_DrawIconsBackground(Geometry);

	// bulb
	BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
	BSP_LCD_FillCircle(Position.X + 3 * Unit, Position.Y + 4 * Unit, 1 * Unit);

	// shade
	BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
	Point Triangle[] = { { 0, 4 }, { 6, 4 }, { 3, 1 } };
	const uint32_t NoVertices = sizeof(Triangle) / sizeof(Triangle[0]);
	for (int i = 0; i < NoVertices; i++) {
		Triangle[i].X *= Unit;
		Triangle[i].X += Position.X;
		Triangle[i].Y *= Unit;
		Triangle[i].Y += Position.Y;
	}
	BSP_LCD_FillPolygon(Triangle, NoVertices);
	BSP_LCD_FillRect(Position.X + 2 * Unit, Position.Y + 1 * Unit, 2 * Unit,
			1 * Unit);

	// wire
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_DrawVLine(Position.X + 3 * Unit, Position.Y + 0 * Unit, 1 * Unit);

	Display_DrawIconsFrame(Geometry);
}

static void Display_DrawIconsBackground(Square_GeometryTypeDef Geometry) {
	BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGRAY);
	BSP_LCD_FillRect(Geometry.Position.X, Geometry.Position.Y, Geometry.Size,
			Geometry.Size);
}

static void Display_DrawIconsFrame(Square_GeometryTypeDef Geometry) {
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_DrawRect(Geometry.Position.X, Geometry.Position.Y, Geometry.Size,
			Geometry.Size);
}

uint32_t Display_GetMaxX() {
	return RK043FN48H_WIDTH;
}

uint32_t Display_GetMaxY() {
	return RK043FN48H_HEIGHT;
}
