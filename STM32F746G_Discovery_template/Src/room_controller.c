/*
 MIT License

 Copyright (c) 2018 Jacek Jankowski

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "room_controller.h"
#include "underbed_light.h"
#include "touchscreen.h"
#include "display.h"
#include "icon.h"
#include "timer.h"

const static uint32_t SCREENSAVER_TIMEOUT = 5000;
static Timer_HandleTypeDef ScreensaverTimer;
static Icon_HandleTypeDef UnderbedLightIcon;

static void RoomController_InitIcons();

void RoomController_Init() {
	UnderbedLight_Init();
	Touchscreen_Init();
	Display_Init();
	Display_Clear();
	RoomController_InitIcons();
	Timer_Restart(&ScreensaverTimer, 0);
}

void RoomController_Run_1ms() {
	static bool WasTimerPrevElapsed = false;
	Timer_Run_1ms(&ScreensaverTimer);
	Touchscreen_Run_1_ms();
	if (Touchscreen_WasScreenPressedDown()) {
		Timer_Restart(&ScreensaverTimer, SCREENSAVER_TIMEOUT);
		if (WasTimerPrevElapsed) {
			Icon_Draw(&UnderbedLightIcon);
		} else {
			Point_GeometryTypeDef TouchedPoint =
					Touchscreen_GetTouchCoordinates();
			Icon_ReactToTouch(&UnderbedLightIcon, TouchedPoint);
		}
	} else if (Timer_IsElapsed(&ScreensaverTimer) && !WasTimerPrevElapsed) {
		Display_Clear();
	}
	WasTimerPrevElapsed = Timer_IsElapsed(&ScreensaverTimer);
}

static void RoomController_InitIcons() {
	Square_GeometryTypeDef IconGeometry;
	IconGeometry.Size = 200;
	IconGeometry.Position.X = (Display_GetMaxX() - IconGeometry.Size) / 2;
	IconGeometry.Position.Y = (Display_GetMaxY() - IconGeometry.Size) / 2;
	Icon_Init(&UnderbedLightIcon, IconGeometry, Display_DrawUnderBedLightIcon,
			UnderbedLight_Toggle);
	Icon_Draw(&UnderbedLightIcon);
}
