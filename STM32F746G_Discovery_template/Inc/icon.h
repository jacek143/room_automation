/*
 MIT License

 Copyright (c) 2018 Jacek Jankowski

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
#ifndef ICON_H_
#define ICON_H_

#include "icon_types.h"

void Icon_Init(Icon_HandleTypeDef* pIcon, Square_GeometryTypeDef Geometry,
		void (*DrawFunction)(Square_GeometryTypeDef), void (*ReactFunction)());
void Icon_Draw(Icon_HandleTypeDef* pIcon);
bool Icon_WasPressedDown(const Icon_HandleTypeDef* pIcon,
		Point_GeometryTypeDef Point);
void Icon_ReactToTouch(Icon_HandleTypeDef* pIcon, Point_GeometryTypeDef Point);

#endif /* ICON_H_ */
